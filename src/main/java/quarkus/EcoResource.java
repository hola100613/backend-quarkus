/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package quarkus;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

//import jakarta.ws.rs.QueryParam;
//import jakarta.ws.rs.PathParam;

@Path("/home")
public class EcoResource {
    
    @GET
    @Produces(MediaType.TEXT_HTML)
    public String home() {
        // Devolver el contenido del archivo index.html
        return "<html><body><h1>Bienvenido a mi página principal</h1></body></html>";
    }

}
