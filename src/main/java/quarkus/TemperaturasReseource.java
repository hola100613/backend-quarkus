package quarkus;


//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collections;
import java.util.List;

//import jakarta.enterprise.inject.Produces;
//import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
//import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.util.NoSuchElementException;

@Path("/temperaturas")
public class TemperaturasReseource {
    
    @Inject
    private TemperaturasService temperaturas;

    public TemperaturasReseource(TemperaturasService temperaturas){
        this.temperaturas = temperaturas;
    }

    @POST
    public Temperatura  nueva(Temperatura temp){
        temperaturas.addTemperatura(temp);
        return temp;
    }

    @GET
    public List<Temperatura> list(){
        return temperaturas.obtenerTemperaturas();
    }

    @GET
    @Path("/maxima")
    public Response maxima(){
        if (temperaturas.isEmpty()){
            return Response.status(404).entity("No hay temperaturas").build();
        }  else{
            int temperaturaMaxima = temperaturas.maxima();
            return Response.ok(Integer.toString(temperaturaMaxima))
            .header("MAXIMA", "TEMPERATURA MAXIMA")
            .build();
        }
    }

    @GET
    @Path("{ciudad}")
    public Temperatura sacar(@PathParam("ciudad") String ciudad){
        return temperaturas.sacarTemperatura(ciudad)
        .orElseThrow(()->new NoSuchElementException("No hay registro para la ciudad" + ciudad));
    }

}
