package quarkus;


import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;

import java.util.List;
import java.util.NoSuchElementException;

import io.quarkus.security.Authenticated;

@Path("/books")
@Transactional
@Authenticated
public class BookResource {
    

   @Inject
   private BookRepository booksRepository;

   @GET
   public List<Book> index(){
        return booksRepository.listAll();
   }
   
   @POST
   public Book insert(Book book){
        booksRepository.persist(book);
        return book;
   }

   @GET
   @Path("{id}")
   public Book retrieve(@PathParam("id") Long id){
     var book = booksRepository.findById(id);
     if (book != null){
          return book;
     }
     throw new NoSuchElementException("No existe libro con la id"+ id + ".");
   }

   @DELETE
   @Path("{id}")
   public String delete(@PathParam("id") Long id){
      if (booksRepository.deleteById(id)){
          return "libro eliminado";
      }
      else{
          return "no se encontro libro ah eliminar";
      }
   }

   @PUT
   @Path ("{id}")
   public Book update(@PathParam("id") Long id, Book book){
     var updateBook = booksRepository.findById(id);
     if (updateBook != null){
          updateBook.setTitle(book.getTitle());
          updateBook.setPubDate(book.getPubDate());
          updateBook.setNumPages(book.getNumPages());
          updateBook.setDescription(book.getDescription());
          booksRepository.persist(updateBook);
     }
     throw new NoSuchElementException("No existe libro con la id"+ id + ".");

     }
}

